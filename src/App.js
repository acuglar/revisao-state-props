import React from 'react';
import logo from './logo.svg';
import './App.css';
import Switch from './components/Switch';
import Lock from './components/Lock';

class App extends React.Component {
  state = {
    showLogo: true,
    locked: false,
  }

  isLogo = () => {
    const { showLogo } = this.state;
    this.setState({ showLogo: !showLogo })
  }

  isLocked = () => {
    const { locked } = this.state;
    this.setState({ locked: !locked })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.state.showLogo && <img src={logo} className="App-logo" alt="logo" />}
          {/* <Switch name="Trocar" trocar={this.isLogo} /> */}  {/* Children to Parent */}
          <Switch locked={this.state.locked} trocar={this.isLogo} showLogo={this.state.showLogo} />  {/* Parent to Children */}
          <Lock locked={this.state.locked} travar={this.isLocked} />
        </header>
      </div>
    );
  }

}

export default App;
