import { Component } from "react";

export default class Lock extends Component {
  render() {
    return (
      <div>
        {!this.props.locked ? (
          <button onClick={this.props.travar}>Travar </button>
        ) : (
          <button onClick={this.props.travar}>Destravar </button>
        )}
      </div>
    );
  }
}
