import { Component } from "react";

export default class Switch extends Component {
  render() {
    return (
      <div>
        {/* <button onClick={this.props.trocar}>{this.props.name}</button> */}
        {this.props.showLogo ? (
          <button onClick={this.props.trocar} disabled={this.props.locked}>
            Esconder
          </button>
        ) : (
          <button onClick={this.props.trocar} disabled={this.props.locked}>
            Mostrar
          </button>
        )}
      </div>
    );
  }
}
